package apigen

import (
	"errors"
	"fmt"
	"path/filepath"
	"strings"
	"text/template"

	"github.com/logrusorgru/aurora"
	"github.com/tal-tech/go-zero/tools/goctl/util"
	"github.com/urfave/cli"
)

const apiTemplate = `syntax = "v1"

info(
	title: 
	desc: 
	author: "{{.gitUser}}"
	email: "{{.gitEmail}}"
)

// 客户端请求参数
type request {
	requestId string ` + "`" + `json:"requestId"` + "`" + `
}

// sql语句需要的参数
type sqlParams struct{
		
}

// 服务端响应数据
type response {
	code    int ` + "`" + `json:"code"` + "`" + `
	message string ` + "`" + `json:"message"` + "`" + `
	data    interface{}  ` + "`" + `json:"data"` + "`" + `
}

service {{.serviceName}} {
	@doc(
		summary :""
	)
	@handler GetUser 
	get /users/id/:userId(request) returns(response)
	
	@doc(
		summary :""
	)
	@handler CreateUser 
	post /users/create(request) returns(response)
}
`

// ApiCommand create api template file
func ApiCommand(c *cli.Context) error {
	apiFile := c.String("o")
	if len(apiFile) == 0 {
		return errors.New("missing -o")
	}

	fp, err := util.CreateIfNotExist(apiFile)
	if err != nil {
		return err
	}
	defer fp.Close()

	home := c.String("home")
	if len(home) > 0 {
		util.RegisterGoctlHome(home)
	}

	text, err := util.LoadTemplate(category, apiTemplateFile, apiTemplate)
	if err != nil {
		return err
	}

	baseName := util.FileNameWithoutExt(filepath.Base(apiFile))
	if strings.HasSuffix(strings.ToLower(baseName), "-api") {
		baseName = baseName[:len(baseName)-4]
	} else if strings.HasSuffix(strings.ToLower(baseName), "api") {
		baseName = baseName[:len(baseName)-3]
	}

	t := template.Must(template.New("etcTemplate").Parse(text))
	if err := t.Execute(fp, map[string]string{
		"gitUser":     GetGitName(),
		"gitEmail":    GetGitEmail(),
		"serviceName": baseName + "-api",
	}); err != nil {
		return err
	}

	fmt.Println(aurora.Green("Done."))
	return nil
}
