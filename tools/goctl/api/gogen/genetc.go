package gogen

import (
	"fmt"
	"strconv"

	"github.com/tal-tech/go-zero/tools/goctl/api/spec"
	"github.com/tal-tech/go-zero/tools/goctl/config"
	"github.com/tal-tech/go-zero/tools/goctl/util/format"
)

const (
	defaultPort = 8888
	etcDir      = "etc"
	etcTemplate = `Name: {{.serviceName}}
Host: {{.host}}
Port: {{.port}}

Database:
  Pkg: "./xml"
  LogPath: "./log.log"
  DriverName: "mysql"
  Host: "127.0.0.1"
  Port: 3306
  User: "root"
  Password: "root"
  DBName: "test"
  MaxOpenConn: 0
  MaxIdleConn: 0
  ConnMaxLifetime: 28800
  ConnMaxIdleTime: 28800

CacheRedis:
  - Host: 127.0.0.1:6379
    Pass: ""
    Type: node

#Auth:
#  AccessSecret: ad87xxxxxxx5d54b7d
#  AccessExpire: 10
`
)

func genEtc(dir string, cfg *config.Config, api *spec.ApiSpec) error {
	filename, err := format.FileNamingFormat(cfg.NamingFormat, api.Service.Name)
	if err != nil {
		return err
	}

	service := api.Service
	host := "0.0.0.0"
	port := strconv.Itoa(defaultPort)

	return genFile(fileGenConfig{
		dir:             dir,
		subdir:          etcDir,
		filename:        fmt.Sprintf("%s.yaml", filename),
		templateName:    "etcTemplate",
		category:        category,
		templateFile:    etcTemplateFile,
		builtinTemplate: etcTemplate,
		data: map[string]string{
			"serviceName": service.Name,
			"host":        host,
			"port":        port,
		},
	})
}
