package gogen

import (
	"bytes"
	"fmt"
	"github.com/tal-tech/go-zero/core/stringx"
	"github.com/tal-tech/go-zero/tools/goctl/api/spec"
	"github.com/tal-tech/go-zero/tools/goctl/config"
	"github.com/tal-tech/go-zero/tools/goctl/util"
	"github.com/tal-tech/go-zero/tools/goctl/util/format"
	"go/ast"
	"go/parser"
	"go/token"
	"html/template"
	"io/ioutil"
	"os"
	"path"
	"path/filepath"
	"runtime/debug"
	"strings"
)

const (
	projectOpenSourceUrl = "github.com/zeromicro/go-zero"
	handlerImports       = `package handler

import (
	"net/http"
	{{.packages}}
)
`

	handlerTemplate     = `func {{.HandlerName}}(ctx *svc.ServiceContext) http.HandlerFunc {
	return func(w http.ResponseWriter, r *http.Request) {
		{{if .HasRequest}}var req types.{{.RequestType}}
		if err := httpx.Parse(r, &req); err != nil {
			httpx.Error(w, err)
			return
		}{{end}}
		l := {{.LogicName}}.New{{.LogicType}}(r.Context(), ctx)
		{{if .HasResp}}resp, {{end}}err := l.{{.Call}}({{if .HasRequest}}req{{end}})
		if err != nil {
			httpx.Error(w, err)
		} else {
			{{if .HasResp}}httpx.OkJson(w, resp){{else}}httpx.Ok(w){{end}}
		}
	}
}
`
)

type Handler struct {
	HandlerName string
	RequestType string
	LogicType   string
	LogicName   string
	Call        string
	HasResp     bool
	HasRequest  bool
}

func genHandlerImports(group spec.Group, route spec.Route, parentPkg string) []string {
	var imports []string
	imports = append(imports, fmt.Sprintf("\"%s\"",
		joinPackages(parentPkg, getLogicFolderPath(group, route))))
	imports = append(imports, fmt.Sprintf("\"%s\"", joinPackages(parentPkg, contextDir)))
	if len(route.RequestTypeName()) > 0 {
		imports = append(imports, fmt.Sprintf("\"%s\"\n", joinPackages(parentPkg, typesDir)))
	}
	imports = append(imports, fmt.Sprintf("\"%s/rest/httpx\"", projectOpenSourceUrl))
	return imports
}

func joinPackages(pkgs ...string) string {
	return strings.Join(pkgs, "/")
}

func getHandlerBaseName(route spec.Route) string {
	handler := route.Handler
	handler = strings.TrimSpace(handler)
	handler = strings.TrimSuffix(handler, "handler")
	handler = strings.TrimSuffix(handler, "Handler")
	return handler
}

func getHandlerFolderPath(group spec.Group, route spec.Route) string {
	folder := route.GetAnnotation(groupProperty)
	if len(folder) == 0 {
		folder = group.GetAnnotation(groupProperty)
		if len(folder) == 0 {
			return handlerDir
		}
	}
	folder = strings.TrimPrefix(folder, "/")
	folder = strings.TrimSuffix(folder, "/")
	return path.Join(handlerDir, folder)
}

func getHandlerName(route spec.Route, folder string) string {
	handler  := getHandlerBaseName(route)
	if folder != handlerDir {
		handler = strings.Title(handler)
	}
	return handler
}

func genHandlers(dir,rootPkg string, cfg *config.Config, api *spec.ApiSpec) error {
	if len(cfg.NamingFormat)==0 {
		cfg.NamingFormat = "gozero"
	}

	for _, group := range api.Service.Groups {
		if len(group.Routes) == 0 {
			continue
		}
		route := group.Routes[0]
		folder := getHandlerFolderPath(group, route) // folder 默认为: internal/handler +"/"+ group.Route 或者 internal/handler
		for _, route := range group.Routes {
			filename,err := format.FileNamingFormat(cfg.NamingFormat, getHandlerName(route, folder))
			if err != nil {
				panic(err)
			}
			filename = filename + ".go"
			os.Remove(filepath.Join(dir, getHandlerFolderPath(group, route), filename))
		}
		err := gen(folder,rootPkg, group, dir, cfg)
		if err != nil {
			debug.PrintStack()
			return err
		}
	}
	return nil
}

func gen(folder ,rootPkg string, group spec.Group, dir string, cfg *config.Config) error {
	filename, err := format.FileNamingFormat(cfg.NamingFormat, "handler")
	if err != nil {
		return err
	}
	filename = filename + ".go" // filename 默认为:handlers.go
	filename = filepath.Join(dir, folder, filename) // /Users/lo/go/src/Practice/go-crud/demo/internal/handler/base/handlers.go (dir 默认为当前项目的绝对路径 + c.String("dir"),folder 默认为: internal/handler +"/"+ group.Route
	dir2 := filepath.Join(dir,folder)
	var (
		fp *os.File
		hasExist = true
	)
	if !pathExist(dir2) {
		err := os.MkdirAll(dir2,os.ModePerm)
		if err != nil {
			return err
		}
		hasExist = false
	}

	fp, err = os.OpenFile(filename, os.O_CREATE|os.O_RDWR, 0600)
	if err != nil {
		return err
	}
	defer fp.Close()

	text, err := util.LoadTemplate("api", "handlers.tpl", handlerTemplate)
	if err != nil {
		return err
	}
	var (
		funcs []string
		imports []string
	)
	for _, route := range group.Routes {
		handler := getHandlerName(route, folder)
		handlerPath := getHandlerFolderPath(group, route)
		pkgName := handlerPath[strings.LastIndex(handlerPath, "/")+1:]
		logicName := "logic"
		if handlerPath != handlerDir {
			handler = strings.Title(handler)
			logicName = pkgName
		}
		if hasExist && funcExist(filename, handler) {
			continue
		}
		handleObj := Handler{
			HandlerName: handler,
			RequestType: strings.Title(route.RequestTypeName()),
			LogicType:   strings.Title(logicName),
			LogicName:   logicName,
			Call:        strings.Title(strings.TrimSuffix(handler, "Handler")),
			HasResp:     len(route.ResponseTypeName()) > 0,
			HasRequest:  len(route.RequestTypeName()) > 0,
		}

		buffer := new(bytes.Buffer)
		err = template.Must(template.New("handlerTemplate").Parse(text)).Execute(buffer, handleObj)
		if err != nil {
			return err
		}

		funcs = append(funcs, buffer.String())

		for _, item := range genHandlerImports(group, route, rootPkg) {
			if !stringx.Contains(imports, item) {
				imports = append(imports, item)
			}
		}
	}

	buffer := new(bytes.Buffer)
	if !hasExist {
		importsStr := strings.Join(imports, "\n\t")
		err = template.Must(template.New("handlerImports").Parse(handlerImports)).Execute(buffer, map[string]string{
			"packages": importsStr,
		})
		if err != nil {
			return err
		}
	}
	formatCode := formatCode(strings.ReplaceAll(buffer.String(), "&#34;", "\"") + strings.Join(funcs, "\n\n"))
	content, err := ioutil.ReadFile(filename)
	if err != nil {
		return err
	}

	if len(content) > 0 {
		formatCode = string(content) + "\n" + formatCode
	}
	_, err = fp.WriteString(formatCode)
	return err
}

func funcExist(filename, funcName string) bool {
	data, err := ioutil.ReadFile(filename)
	if err != nil {
		return false
	}
	set := token.NewFileSet()
	packs, err := parser.ParseFile(set, filename, string(data), parser.ParseComments)
	if err != nil {
		panic(err)
	}
	for _, d := range packs.Decls {
		if fn, isFn := d.(*ast.FuncDecl); isFn {
			if fn.Name.String() == funcName {
				return true
			}
		}
	}
	return false
}

// 返回 true 说明文件或文件夹存在
func pathExist(path string) bool {
	_, err := os.Stat(path)    //os.Stat获取文件信息
	if os.IsNotExist(err) {
		return false
	}
	return true
}
