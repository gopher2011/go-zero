package gogen

import (
	"fmt"
	"path"
	"strings"

	"github.com/tal-tech/go-zero/tools/goctl/api/spec"
	"github.com/tal-tech/go-zero/tools/goctl/config"
	ctlutil "github.com/tal-tech/go-zero/tools/goctl/util"
	"github.com/tal-tech/go-zero/tools/goctl/util/format"
	"github.com/tal-tech/go-zero/tools/goctl/vars"
)

const logicTemplate = `package {{.pkgName}}

import (
	{{.imports}}
 	"github.com/gogf/gf/util/gvalid"
	"github.com/gogf/gf/v2/util/gconv"
)

func (l *{{.logic}}) {{.function}}({{.request}}) {{.responseType}} {
	if err := gvalid.CheckStruct(l.Ctx, &req, nil); err != nil {
		return nil,err
	}
	var arg types.Request
	if err := gconv.Struct(req, &arg); err != nil {
		return nil, err
	}
	{{.returnString}}
}
`

const logicBaseTemplate = `package {{.pkgName}}

import (
	{{.imports}}
)

type {{.logic}} struct {
	logx.Logger
	Ctx    context.Context
	SvcCtx *svc.ServiceContext
}

func New{{.logic}}(ctx context.Context, svcCtx *svc.ServiceContext)*{{.logic}}{
	return &{{.logic}}{
		Logger: logx.WithContext(ctx),
		Ctx:    ctx,
		SvcCtx: svcCtx,
	}
}
`

func genLogic(dir, rootPkg string, cfg *config.Config, api *spec.ApiSpec) error {
	for _, g := range api.Service.Groups {
		for _, r := range g.Routes {
			genBaseLogic(dir, rootPkg,g, r)
			err := genLogicByRoute(dir, rootPkg, cfg, g, r)
			if err != nil {
				return err
			}
		}
	}
	return nil
}

func genLogicByRoute(dir, rootPkg string, cfg *config.Config, group spec.Group, route spec.Route) error {
	logic := getHandlerBaseName(route)
	goFile, err := format.FileNamingFormat(cfg.NamingFormat, logic)
	if err != nil {
		return err
	}

	var (
		responseString string
		returnString string
		requestString string
	)
	if len(route.ResponseTypeName()) > 0 {
		resp := responseGoTypeName(route, typesPacket)
		responseString = "(" + resp + ", error)"
		if strings.HasPrefix(resp, "*") {
			returnString = fmt.Sprintf("return &%s{}, nil", strings.TrimPrefix(resp, "*"))
		} else {
			returnString = fmt.Sprintf("return %s{}, nil", resp)
		}
	} else {
		responseString = "error"
		returnString = "return nil"
	}
	if len(route.RequestTypeName()) > 0 {
		requestString = "req " + requestGoTypeName(route, typesPacket)
	}
	imports := genLogicImports(route, rootPkg)
	subDir,ok := getLogicFolderPath2(group, route)
	pkgName := subDir[strings.LastIndex(subDir, "/")+1:]
	fileGen := fileGenConfig{
		dir:             dir,
		subdir:          subDir,
		filename:        goFile + ".go",
		templateName:    "logicTemplate",
		category:        category,
		templateFile:    logicTemplateFile,
		builtinTemplate: logicTemplate,
	}
	if !ok {
		fileGen.data = map[string]string{
			"pkgName":      "logic",
			"imports":      imports,
			"logic":        strings.Title("logic"),
			"function":     strings.Title(logic),
			"responseType": responseString,
			"returnString": returnString,
			"request":      requestString,
		}
	}else {
		fileGen.data = map[string]string{
			"pkgName": pkgName,
			"imports": imports,
			"logic":   strings.Title(pkgName),
			"function":     strings.Title(logic),
			"responseType": responseString,
			"returnString": returnString,
			"request":      requestString,
		}
	}
	return genFile(fileGen)
}

func getLogicFolderPath(group spec.Group, route spec.Route) string {
	folder := route.GetAnnotation(groupProperty)
	if len(folder) == 0 {
		folder = group.GetAnnotation(groupProperty)
		if len(folder) == 0 {
			return logicDir
		}
	}
	folder = strings.TrimPrefix(folder, "/")
	folder = strings.TrimSuffix(folder, "/")
	return path.Join(logicDir, folder)
}

func genLogicImports(route spec.Route, parentPkg string) string {
	var imports []string
	if len(route.ResponseTypeName()) > 0 || len(route.RequestTypeName()) > 0 {
		imports = append(imports, fmt.Sprintf("\"%s\"\n", ctlutil.JoinPackages(parentPkg, typesDir)))
	}
	return strings.Join(imports, "\n\t")
}

func genBaseLogic(dir, rootPkg string,group spec.Group, route spec.Route)error{
	imports := genLogicImports2(rootPkg)
	subDir,ok := getLogicFolderPath2(group, route)
	pkgName := subDir[strings.LastIndex(subDir, "/")+1:]
	fileGen := fileGenConfig{
		dir:             dir,
		subdir:          subDir,
		filename:        pkgName + ".go",
		templateName:    "logicTemplate",
		category:        category,
		templateFile:    logicTemplateFile,
		builtinTemplate: logicBaseTemplate,
	}
	if !ok {
		fileGen.data = map[string]string{
			"pkgName": "logic",
			"imports": imports,
			"logic":   strings.Title("logic"),
		}
	}else {
		fileGen.data = map[string]string{
			"pkgName": pkgName,
			"imports": imports,
			"logic":   strings.Title(pkgName),
		}
	}
	return genFile(fileGen)
}

func genLogicImports2(parentPkg string) string {
	var imports []string
	imports = append(imports, `"context"`)
	imports = append(imports, fmt.Sprintf("\"%s\"", ctlutil.JoinPackages(parentPkg, contextDir)))
	imports = append(imports, fmt.Sprintf("\"%s/core/logx\"", vars.ProjectOpenSourceURL))
	return strings.Join(imports, "\n\t")
}

func getLogicFolderPath2(group spec.Group, route spec.Route) (string,bool) {
	folder := route.GetAnnotation(groupProperty)
	if len(folder) == 0 {
		folder = group.GetAnnotation(groupProperty)
		if len(folder) == 0 {
			return logicDir,false
		}
	}
	folder = strings.TrimPrefix(folder, "/")
	folder = strings.TrimSuffix(folder, "/")
	return path.Join(logicDir, folder),true
}

